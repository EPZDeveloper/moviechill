package com.cb.epz.epzmoviechill.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.cb.epz.epzmoviechill.R;
import com.cb.epz.epzmoviechill.presenter.LoginPresenter;
import com.cb.epz.epzmoviechill.presenter.LoginPresenterImpl;
import com.cb.epz.epzmoviechill.view.LoginView;

public class LoginActivity extends AppCompatActivity implements LoginView
{
    private LoginPresenter presenter;
    private AppCompatEditText edtUserName;
    private AppCompatEditText edtPassword;
    private Button buttonLogin;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);
        getSupportActionBar().hide();
        initializeView();
        presenter = new LoginPresenterImpl(this);
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.handleLogin(edtUserName.getText().toString(), edtPassword.getText().toString());
            }
        });
    }
    private void initializeView()
    {
        edtUserName = findViewById(R.id.edtUserName);
        edtPassword = findViewById(R.id.edtPwd);
        buttonLogin = findViewById(R.id.buttonLogin);
    }
    @Override
    public void showValidationErrorMsg()
    {
        Toast.makeText(this, "Username or Password is incorrect", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void loginSuccessFully()
    {
        Toast.makeText(this, "Login SuccessFully", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void loginFail()
    {
        Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
    }
}
