package com.cb.epz.epzmoviechill.presenter;

public interface LoginPresenter {

    /*when user click on login button from Activity*/
    void handleLogin(String username, String password);
}
