package com.cb.epz.epzmoviechill.view;

public interface LoginView {

    void showValidationErrorMsg();
    void loginSuccessFully();
    void loginFail();

}
