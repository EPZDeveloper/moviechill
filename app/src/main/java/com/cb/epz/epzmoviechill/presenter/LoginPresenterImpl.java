package com.cb.epz.epzmoviechill.presenter;

import android.text.TextUtils;

import com.cb.epz.epzmoviechill.view.LoginView;

public class LoginPresenterImpl implements LoginPresenter
{
    private LoginView loginView;

    public LoginPresenterImpl(LoginView loginView)
    {
        this.loginView = loginView;
    }
    @Override
    public void handleLogin(String username, String password)
    {
        if ((TextUtils.isEmpty(username) || TextUtils.isEmpty(password)))
        {
            loginView.showValidationErrorMsg();
        }
        else
        {
            if ((username.equals("epz") && password.equals("123")))
            {
                loginView.loginSuccessFully();
            }
            else
            {
                loginView.loginFail();
            }
        }

    }

}
